//1
#include <iostream>

using namespace std;

template <typename T>
class Rational
{
	public:
		T numerator;
		T denominator;
		Rational(const T numer = 0, const T denom = 0);
		Rational operator+ (const Rational &rat) const;
		Rational operator- () const;
		Rational operator- (const Rational &rat) const;
		Rational operator* (const Rational &rat) const;
		Rational operator/ (const Rational &rat) const;
};

template <typename T>
ostream &operator<< (ostream &os, const Rational<T> &rat);

int main()
{
	Rational<double> rat1(1,2);
	Rational<double> rat2(1,3);
	Rational<double> ratRes;

	// ratRes = rat1 + rat2;
	// ratRes = rat1 - rat2;
	// ratRes = rat1 * rat2;
	// ratRes = rat1 / rat2;

	cout << ratRes << endl;
	cin.get();
}

template <typename T>
Rational<T>::Rational(const T numer, const T denom )
        :numerator(numer),
         denominator(denom)
{
}

template <typename T>
Rational<T> Rational<T>::operator+ (const Rational &rat) const
{
	return Rational<T>(numerator * rat.denominator +
	                   rat.numerator * denominator,
	                   denominator * rat.denominator);
}

template <typename T>
Rational<T> Rational<T>::operator- () const
{
	return Rational<T>(-numerator, denominator);
}

template <typename T>
Rational<T> Rational<T>::operator- (const Rational &rat) const
{
	return operator+( rat.operator-() );
}

template <typename T>
Rational<T> Rational<T>::operator* (const Rational &rat) const
{
	return Rational<T>(numerator*rat.numerator, denominator*rat.denominator);
}

template <typename T>
Rational<T> Rational<T>::operator/ (const Rational &rat) const
{
	return operator*(Rational<T>(rat.denominator, rat.numerator));
}

template <typename T>
ostream &operator<< (ostream &os, const Rational<T> &rat)
{
	return os << rat.numerator << '/' << rat.denominator;
}
