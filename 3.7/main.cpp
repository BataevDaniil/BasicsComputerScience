#include <iostream>
#include <ctime>
#include <fstream>

using namespace std;

#define show(x) cout << #x << " = " << x << endl

int main()
{
	const int N = 1e6;
	show(N);
	int a[N], b[N];

	for (int i = 0; i < N; i++)
		a[i] = b[i] = 1;

	clock_t begin;
	double sum;
	ofstream write("out.txt");

	begin = clock();
	sum = 0;
	for (int i = 0; i < N; i++)
	{
		sum += a[i] / b[i];
	}
	write << "1 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/2; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "2 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/3; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "3 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/4; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "4 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/5; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "5 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/6; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "6 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/7; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "7 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/8; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "8 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/9; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "9 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/10; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "10 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/11; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "11 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/12; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "12 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/13; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "13 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/14; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "14 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/15; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "15 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/16; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "16 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/17; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "17 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/18; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "18 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/19; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "19 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/20; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "20 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/21; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "21 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/22; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "22 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/23; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "23 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/24; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "24 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/25; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "25 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/26; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "26 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/27; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "27 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/28; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "28 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/29; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "29 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/30; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "30 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/31; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "31 " << (clock() - begin) << endl;

	begin = clock();
	sum = 0;
	for (int i = 0; i < N/32; i++)
	{
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
		sum += a[i] / b[i];
	}
	write << "32 " << (clock() - begin) << endl;
	return 0;
}