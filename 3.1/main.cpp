#include <iostream>
#include <functional>
#include <fstream>
#include <ctime>

#define show(x) std::cout << #x << " = " << x << std::endl;
//------------------------------------------------------------------------------

enum TypeEnter
{
	INCREASE_VALUE_ARR,
	DECREASE_VALUE_ARR,
	ALTERNATELY_VALUE_ARR
};

template <typename T>
class TestSort
{
	private:
		T *arr;
		int length;
	public:
		TestSort<T>(int len):length(len)
		{
			arr = new T[len];
		};

		TestSort<T>(int len, int typeEnter):length(len)
		{
			arr = new T[len];
			enter(typeEnter);
		};

		TestSort<T>(int len, std::function<T(int)> f):length(len)
		{
			arr = new T[len];
			enter(f);
		};

		void enter(std::function<T(int)> f)
		{
			for (int i = 0; i < length; i++)
				arr[i] = f(i);
		};

		void enter(int typeEnter)
		{
			switch (typeEnter)
			{
				case INCREASE_VALUE_ARR:
					for (int i = 0; i < length; i++)
						arr[i] = i;
				break;

				case DECREASE_VALUE_ARR:
					for (int i = 0; i < length; i++)
						arr[i] = length - i - 1;
				break;

				case ALTERNATELY_VALUE_ARR:
					for (int i = 0; i < length; i++)
						if (i % 2)
							arr[i] = length - i;
						else
							arr[i] = i;
				break;

			};
		};

		void writeTerminal(int begin, int end)
		{
			for (int i = 0; i < begin; i++)
				std::cout << arr[i] << " ";
			std::cout << "| ";
			for (int i = length - end; i < length; i++)
				std::cout << arr[i] << " ";
			std::cout << std::endl;
		};

		void writeTerminal()
		{
			for (int i = 0; i < length; i++)
				std::cout << arr[i] << " ";
			std::cout << std::endl;
		};

		void writeFile(std::ofstream &ofs, int begin, int end)
		{
			for (int i = 0; i < begin; i++)
				ofs << arr[i] << " ";
			ofs << "| ";
			for (int i = length - end; i < length; i++)
				ofs << arr[i] << " ";
			ofs << std::endl;
		};

		int getLegth()
		{
			return length;
		};

		void changeElemArray(int len, T x)
		{
			arr[len] = x;
		};

		void changeLengthArray(int len)
		{
			delete arr;
			arr = new T[length = len];
		};

		double startSort(void (*fSort)(T*, int))
		{
			double begin = clock();
			fSort(arr, length);
			return clock() - begin;
		};
};
//==============================================================================

template <typename T>
void bubbleSort(T *arr, int len);

template <typename T>
void bubbleSort1(T *arr, int len);

template <typename T>
void choiceArrSort(T *arr, int len);

template <typename T>
void ensertSort(T *arr, int len);

template <typename T>
void ensertQuickSort(T *arr, int len);

template <typename T>
int dihotom(T *arr, int len, T x);

void inputFile();

//==============================================================================

int beginArr, endArr;
int countElements;
int typeEnter;

//==============================================================================

int main()
{
	inputFile();
	std::ofstream writeFile("out.txt", std::ofstream::out);

	TestSort<int> testSort(countElements);

	auto tamplat = [&](const char *str, void (*fSort)(int*, int))
	{
		testSort.enter(typeEnter);
		testSort.changeElemArray(2, -1);
		testSort.writeFile(writeFile, beginArr, endArr);
		writeFile << str <<  testSort.startSort(fSort) << std::endl;
		testSort.writeFile(writeFile, beginArr, endArr);
		writeFile << std::endl;
	};
	tamplat("timeBubleSort = ", bubbleSort);
	tamplat("timeBubleSort1 = ", bubbleSort1);
	// tamplat("timeChoiceArrSort = ", choiceArrSort);
	// tamplat("timeEnsertSort = ", ensertSort);
	// tamplat("timeEnsertQuickSort = ", ensertQuickSort);

	writeFile.close();
};
//==============================================================================

template <typename T>
int dihotom(T *arr, int len, T x)
{
	int l = -1;
	int r = len;
	while(r - l > 1)
	{
		int mid = (r + l)/2;
		if (arr[mid] > x)
			r = mid;
		else
			l = mid;
	}
	return r;
};
//==============================================================================

template <typename T>
void ensertQuickSort(T *arr, int len)
{
	for (int i = 1; i < len; i++)
	{
		T tmp = arr[i];
		int index = dihotom(arr, i, arr[i]);
		for (int k = i; k > index; k--)
			arr[k] = arr[k-1];
		arr[index] = tmp;
	}
};
//==============================================================================

template <typename T>
void ensertSort(T *arr, int len)
{
	for (int i = 1; i < len; i++)
		for (int j = 0; j < i; j++)
			if (arr[i] < arr[j])
			{
				T tmp = arr[i];
				for (int k = i; k > j; k--)
					arr[k] = arr[k-1];
				arr[j] = tmp;
			}
};
//==============================================================================

template <typename T>
void choiceArrSort(T *arr, int len)
{
	for (int i = 0; i < len; i++)
	{
		int min = arr[i];
		int indexMin = i;
		for (int j = i + 1; j < len; j++)
			if (min > arr[j])
				min = arr[j], indexMin = j;
		std::swap(arr[i], arr[indexMin]);
	};
};
//==============================================================================
template <typename T>
void bubbleSort(T *arr, int len)
{
	for (int i = 0; i < len; i++)
	{
		bool flag = true;
		for (int j = 0, cnt = len - i - 1; j < cnt; j++)
			if (arr[j] > arr[j+1])
			{
				std::swap(arr[j], arr[j+1]);
				flag = false;
			}
		if (flag)
			break;
	};
};
//==============================================================================

template <typename T>
void bubbleSort1(T *arr, int len)
{
	for (int i = len; --i;)
	{
		// for (int j = len - i; j >= 0; j--)
		for (int j = len - i; j--;)
			if (arr[j] > arr[j+1])
				std::swap(arr[j], arr[j+1]);
	};
};
//==============================================================================

void inputFile()
{
	std::ifstream readFile("input.txt", std::ofstream::in);
	if (!readFile.is_open())
		std::cout << "error read file" << std::endl, exit(1);

	char buf[100];
	readFile >> buf; readFile >> countElements;
	show(countElements);

	readFile >> buf; readFile >> buf; readFile >> buf;
	readFile >> buf; readFile >> endArr;
	show(endArr);

	readFile >> buf; readFile >> beginArr;
	show(beginArr);

	readFile >> buf; readFile >> typeEnter;
	show(typeEnter);
	readFile.close();
};
//==============================================================================