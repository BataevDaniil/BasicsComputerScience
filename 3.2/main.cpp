#include <iostream>
#include <functional>
#include <fstream>
#include <ctime>

#define show(x) std::cout << #x << " = " << x << std::endl;
//------------------------------------------------------------------------------

enum TypeEnter
{
	INCREASE_VALUE_ARR,
	DECREASE_VALUE_ARR,
	ALTERNATELY_VALUE_ARR
};

template <typename T>
class TestSort
{
	private:
		T *arr;
		int length;
	public:
		TestSort<T>(int len):length(len)
		{
			arr = new T[len];
		};

		TestSort<T>(int len, int typeEnter):length(len)
		{
			arr = new T[len];
			enter(typeEnter);
		};

		TestSort<T>(int len, std::function<T(int)> f):length(len)
		{
			arr = new T[len];
			enter(f);
		};

		void enter(std::function<T(int)> f)
		{
			for (int i = 0; i < length; i++)
				arr[i] = f(i);
		};

		void enter(int typeEnter)
		{
			switch (typeEnter)
			{
				case INCREASE_VALUE_ARR:
					for (int i = 0; i < length; i++)
						arr[i] = i;
				break;

				case DECREASE_VALUE_ARR:
					for (int i = 0; i < length; i++)
						arr[i] = length - i - 1;
				break;

				case ALTERNATELY_VALUE_ARR:
					for (int i = 0; i < length; i++)
						if (i % 2)
							arr[i] = length - i;
						else
							arr[i] = i;
				break;

			};
		};

		void writeTerminal(int begin, int end)
		{
			for (int i = 0; i < begin; i++)
				std::cout << arr[i] << " ";
			std::cout << "| ";
			for (int i = length - end; i < length; i++)
				std::cout << arr[i] << " ";
			std::cout << std::endl;
		};

		void writeTerminal()
		{
			for (int i = 0; i < length; i++)
				std::cout << arr[i] << " ";
			std::cout << std::endl;
		};

		void writeFile(std::ofstream &ofs, int begin, int end)
		{
			for (int i = 0; i < begin; i++)
				ofs << arr[i] << " ";
			ofs << "| ";
			for (int i = length - end; i < length; i++)
				ofs << arr[i] << " ";
			ofs << std::endl;
		};

		int getLegth()
		{
			return length;
		};

		void changElemArr(int i, T a)
		{
			arr[i] = a;
		};

		void changeLengthArray(int len)
		{
			delete arr;
			arr = new T[length = len];
		};

		double startSort(void (*fSort)(T*, int))
		{
			double begin = clock();
			fSort(arr, length);
			return clock() - begin;
		};
};
//==============================================================================

void inputFile();

template <typename T>
void quickSort(T *arr, int l, int r);

template <typename T>
void quickSort1(T *arr, int len);

template <typename T>
void quickSortPivEnd1(T *arr, int len);

template <typename T>
void quickSortPivEnd(T *arr, int l, int r);

template <typename T>
void margeSortRecurs(T *arr, int l, int r);

template <typename T>
void margeSortRecurs1(T *arr, int len);

template <typename T>
void marge(T *arr, int l, int mid, int r);

template <typename T>
void margeSortIterac(T *arr, int len);

//==============================================================================

int beginArr, endArr;
int countElements;
int typeEnter;

//==============================================================================

int main()
{
	inputFile();
	std::ofstream writeFile("out.txt", std::ofstream::out);

	TestSort<int> testSort(countElements);

	auto tamplat = [&](const char *str, void (*fSort)(int*, int))
	{
		testSort.enter(typeEnter);
		// testSort.changeLengthArray(14);
		// testSort.enter([&](int i) -> int
		// {
		// 	// int tmp[] = {1,3,4,6,8,2,5,7,2,566,23};
		// 	int tmp[] = {2,123,4,244,544,2, 8, 9, 0, 12, 32, 312, 42, 25, 321, 4245, 21324};
		// 	return tmp[i];
		// });
		testSort.writeFile(writeFile, beginArr, endArr);
		// testSort.changElemArr(3, 2);
		// testSort.changElemArr(5, 2);
		// testSort.changElemArr(7, 2);
		// testSort.writeTerminal(20, 10);
		// testSort.writeTerminal();
		writeFile << str <<  testSort.startSort(fSort) << std::endl;
		// testSort.writeTerminal();
		// testSort.writeTerminal(10, 10);
		testSort.writeFile(writeFile, beginArr, endArr);
		writeFile << std::endl;
	};
	tamplat("timeQuickSort = ", quickSort1);
	tamplat("timeQuickSortPivEnd = ", quickSortPivEnd1);
	tamplat("timeMargeSortRecurs = ", margeSortRecurs1);
	tamplat("timeMargeSortIterac = ", margeSortIterac);

	writeFile.close();
};
//==============================================================================

template <typename T>
void marge(T *arr, int l, int mid, int r)
{
	T tmp[r - l];
	int i = l, j = mid, k = 0;
	for (; i < mid && j < r; k++)
		tmp[k] = (arr[i] < arr[j])?arr[i++]: arr[j++];

	for (; i < mid; i++, k++)
			tmp[k] = arr[i];
	for (; j < r; j++, k++)
			tmp[k] = arr[j];
	for (int i = l; i < r; i++)
		arr[i] = tmp[i - l];
};
//==============================================================================

template <typename T>
void margeSortIterac(T *arr, int len)
{
	int end = (len % 2)? len-1: len;
	for (int i = 1; i < end; i *= 2)
	{
		for (int j = 0; j < end; j += i*2)
			marge(arr, j, j + i, j + i*2);
		if (end % (i*4))
		{
			end -= i*2;
			marge(arr, end, end + i*2, len);
		}
	}
};
//==============================================================================

template <typename T>
void margeSortRecurs1(T *arr, int len)
{
	margeSortRecurs(arr, 0, len);
};

//==============================================================================

template <typename T>
void margeSortRecurs(T *arr, int l, int r)
{
	if (l < r - 1)
	{
		int mid = (l + r)/2;
		margeSortRecurs(arr, l, mid);
		margeSortRecurs(arr, mid, r);
		marge(arr, l, mid, r);
	}
};
//==============================================================================

template <typename T>
void quickSortPivEnd1(T *arr, int len)
{
	quickSortPivEnd(arr, 0, len-1);
};
//==============================================================================

template <typename T>
void quickSortPivEnd(T *arr, int l, int r)
{
	int piv = arr[r];
	int i = l, j = r;
	while (i <= j)
	{
		while (arr[i] < piv)
			i++;
		while (arr[j] > piv)
			j--;
		if (i <= j)
		{
			std::swap(arr[i], arr[j]);
			i++, j--;
		}
	}
	if (i < r) quickSort(arr, i, r);
	if (j > l) quickSort(arr, l, j);
};
//==============================================================================

template <typename T>
void quickSort1(T *arr, int len)
{
	quickSort(arr, 0, len-1);
};
//==============================================================================

template <typename T>
void quickSort(T *arr, int l, int r)
{
	int piv = arr[(r + l)/2];
	int i = l, j = r;
	while (i <= j)
	{
		while (arr[i] < piv)
			i++;
		while (arr[j] > piv)
			j--;
		if (i <= j)
		{
			std::swap(arr[i], arr[j]);
			i++, j--;
		}
	}
	if (i < r) quickSort(arr, i, r);
	if (j > l) quickSort(arr, l, j);
};
//==============================================================================

void inputFile()
{
	std::ifstream readFile("input.txt", std::ofstream::in);
	if (!readFile.is_open())
		std::cout << "error read file" << std::endl, exit(1);

	char buf[100];
	readFile >> buf; readFile >> countElements;
	show(countElements);

	readFile >> buf; readFile >> buf; readFile >> buf;
	readFile >> buf; readFile >> endArr;
	show(endArr);

	readFile >> buf; readFile >> beginArr;
	show(beginArr);

	readFile >> buf; readFile >> typeEnter;
	show(typeEnter);
	readFile.close();
};
//==============================================================================
