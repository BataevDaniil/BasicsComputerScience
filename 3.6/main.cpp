#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <algorithm>

#define show(x) cout << #x << " = " << x << endl

using namespace std;

template<typename T>
class List
{
	public:
		struct container
		{
			T data;
			container *next;
		};

	private:
		container *head;
		container *tail;
		int count;
	public:
		List();
		void push_back(T tmp);
		T &getElem(int n);
		int countElem();
		container *remove(T &elem);
};

void readFile();
void writeFile();

struct Name
{
	string name;
	string lastName;
	bool operator == (Name &tmp)
	{
		return name == tmp.name;
	}
};

List<Name> list;

int main()
{
	readFile();
	list.remove( list.getElem(0) );
	writeFile();
}

void writeFile()
{
	ofstream write("out.txt");
	write << "count= " << list.countElem() << endl;
	for (int i = 0; i < list.countElem(); i++)
		write << list.getElem(i).name << ' ' << list.getElem(i).lastName << endl;
	write.close();
}

void readFile()
{
	ifstream read("input.txt");
	char buff[200];
	int countElem;
	read >> buff; read >> countElem;
	for (int i = 0; i < countElem; i++)
	{
		string tmp1, tmp2;
		read >> tmp1; read >> tmp2;
		list.push_back(Name{tmp1, tmp2});
	}
	read.close();
}

template<typename T>
List<T>::List():
	head(NULL),
	tail(NULL),
	count(0)
{
}

template<typename T>
void List<T>::push_back(T tmp)
{
	if (tail)
	{
		tail->next = new container;
		tail = tail->next;
		tail->data = tmp;
		tail->next = NULL;
	}
	else
	{
		tail = new container;
		tail->data = tmp;
		tail->next = NULL;
		head = tail;
	}
	count++;
}

template<typename T>
T &List<T>::getElem(int n)
{
	container *tmp = head;
	for (int i = 0; i < n; i++)
		tmp = tmp->next;
	return tmp->data;
}

template<typename T>
int List<T>::countElem()
{
	return count;
}

template<typename T>
typename List<T>::container *List<T>::remove(T &elem)
{
	container *now = head;
	container *tmpTail = tail;
	for (container *prev = NULL; now != tmpTail;)// prev = now, now = now->next)
	{
		if (now->data == elem)
		{
			container *next = now->next;
			if (prev)
				prev->next = next;
			else
				head = next;
			tail->next = now;
			tail = now;
			tail->next = NULL;
			now = next;
		}
		else
		{
			prev = now;
			now = now->next;
		}
	}
	return (elem == tmpTail->data)?tmpTail: tmpTail->next;
}
