#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <array>
#include <algorithm>

#define DEBUG

using namespace std;

#define show(x) cout << #x << " = " << x << endl

class Journal
{
	public:

		struct Student;

	private:
		vector<Student> data;
		const int CNTDAY;
		const int CNTTASK;
		int skipeWord(ifstream &read, int n);
	public:
		Journal(const char *fileName, const int cntday = 9, const int cnttask = 9);

		struct Task
		{
			string current;
			bool Yn;
		};

		struct Student
		{
			string name;
			string lastName;
			vector<bool> skip;
			vector<Task> task;
		};
		void readFile(const char *fileName);
		vector<string> *nLnSkip(int n);
		void writeFile(const char *path);
};


int main()
{
	Journal journal("/media/daniil/DC16837F16835982/Users/daniil/Documents/BasicsComputerScience/3.5/journal.txt");
	journal.writeFile("/media/daniil/DC16837F16835982/Users/daniil/Documents/BasicsComputerScience/3.5/");
}

int Journal::skipeWord(ifstream &read, int n)
{
	string trash;
	char ans;
	for (int i = 0; i < n && read.good(); i++)
	{
		read >> trash;
#ifdef DEBUG
		show(trash);
#endif
	}
	return read.good();
}

void Journal::readFile(const char *fileName)
{
	ifstream read(fileName);
	if (!read.is_open())
	{
		show("file not read");
		return;
	}
	{
		while (read.get() != '\n')
			;
		for (int i = 0; skipeWord(read, 1); i++)
		{

			data.push_back(Student{});
			read >> data[i].name;
			read >> data[i].lastName;
			string tmp;
			read >> tmp;
			if (tmp[0] == -30)
				skipeWord(read, 1), read >> tmp;

			for (int j = 0; j < CNTDAY-1; j++)
			{
				if (tmp[0] == '-' || tmp[0] == -48)
					data[i].skip.push_back(false);
				else if (tmp[0] == '+')
					data[i].skip.push_back(true);
				read >> tmp;
			}

			for (int j = 0; j < CNTTASK; j++)
			{
				read >> tmp;
				data[i].task.push_back(Task{tmp, (tmp[tmp.length() - 1] == '+')});
			}
#ifdef DEBUG
			show(data[i].name);
			show(data[i].lastName);
			for (auto skip: data[i].skip)
				cout << skip << ' ';
			cout << endl;
			for (auto task: data[i].task)
				cout << task.current << task.Yn << ' ';
			cout << endl;
#endif
		}
	}
	read.close();
}

Journal::Journal(const char *fileName, const int cntday, const int cnttask):
	CNTDAY(cntday),
	CNTTASK(cnttask)
{
	readFile(fileName);
}

void Journal::writeFile(const char *path)
{
	string tmp = path;
	tmp += "out.txt";
	ofstream write(tmp.c_str());
	show(tmp);
	vector<string> *skip = nLnSkip(3);
	for (auto x: *skip)
		write << x << endl;
	write.close();
}

vector<string> *Journal::nLnSkip(int n)
{
	vector<string> *skip = new vector<string>;
	for (auto stude: data)
	{
		int sum = 0;
		for (auto x: stude.skip)
			sum += x;
		if (sum > n)
			(*skip).push_back(stude.name + " " + stude.lastName);
	}
	return skip;
}
