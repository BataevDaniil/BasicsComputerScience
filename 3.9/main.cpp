#include <iostream>
#include <windows.h>

using namespace std;

#define DEBUG

class BMP
{
	private:
		BITMAPFILEHEADER bmpfh;
		BITMAPINFOHEADER bmpih;

		void readImage(const char *FileName);
	public:
		unsigned char *R, *G, *B;
		int height, width;
		int length;

		BMP(const char *fileName);

		void writeImage(const char *FileName);
};

int main()
{
	// BMP image("../output.bmp");
	// image.writeImage("../out.bmp");
	BMP image("out.bmp");
	for (int i = 0; i < image.length; i++)
		image.B[i] -= 40;
	image.writeImage("input.bmp");
}

BMP::BMP(const char *fileName)
{
	readImage(fileName);
}
void BMP::readImage(const char *FileName)
{
	FILE *file = fopen(FileName,"rb");

	fread(&bmpfh,sizeof(BITMAPFILEHEADER),1,file);
#ifdef DEBUG
	printf("bmpfh.bfOffBits = %d\n",bmpfh.bfOffBits);
	printf("bmpfh.bfReserved1 =  %d\n",bmpfh.bfReserved1);
	printf("bmpfh.bfReserved2 =  %d\n",bmpfh.bfReserved2);
	printf("bmpfh.bfSize =  %d\n",bmpfh.bfSize);
	printf("bmpfh.bfType = %d\n",bmpfh.bfType);
#endif
	fread(&bmpih,sizeof(BITMAPINFOHEADER),1,file);
	height = bmpih.biHeight;
	width = bmpih.biWidth;

	length = height * width;

	R = new unsigned char[length];
	G = new unsigned char[length];
	B = new unsigned char[length];

#ifdef DEBUG
	printf("bmpih.biBitCount = %d\n",bmpih.biBitCount);
	printf("bmpih.biHeight = %d\n",bmpih.biHeight);
	printf("bmpih.biWidth = %d\n",bmpih.biWidth);
#endif

	//printf("%d\n",ftell(file));
	fseek(file,bmpfh.bfOffBits,SEEK_SET);
	//printf("%d\n",ftell(file));

	for(int i = 0; i<bmpih.biHeight; i++)
	{
		for(int j = 0; j<bmpih.biWidth; j++)
		{
			fread(B+i*bmpih.biWidth+j, 1, 1, file);
			fread(G+i*bmpih.biWidth+j, 1, 1, file);
			fread(R+i*bmpih.biWidth+j, 1, 1, file);
		}
		for(int k = 3*bmpih.biWidth%4; k!=0; k = (k+1)%4)
		{
			char ctemp = 0;
			fread(&ctemp,1,1,file);
		}
	}
	fclose(file);
}

void BMP::writeImage(const char *FileName)
{
	FILE *file = fopen(FileName,"wb");

	fwrite(&bmpfh,sizeof(BITMAPFILEHEADER),1,file);
	fwrite(&bmpih,sizeof(BITMAPINFOHEADER),1,file);

	for(int i = 0; i<bmpih.biHeight; i++)
	{
		for(int j = 0;j<bmpih.biWidth; j++)
		{
			fwrite(B+i*bmpih.biWidth+j,1,1,file);
			fwrite(G+i*bmpih.biWidth+j,1,1,file);
			fwrite(R+i*bmpih.biWidth+j,1,1,file);
		}
		for(int k = 3*bmpih.biWidth%4; k!=0; k = (k+1)%4)
		{
			char ctemp = 0;
			fwrite(&ctemp,1,1,file);
		}
	}
	if(ftell(file) < bmpfh.bfSize)
	{
		printf("%d\n",bmpfh.bfSize - ftell(file));
		char ctemp = 0;
		for(int counter = bmpfh.bfSize - ftell(file); counter>0; counter--) fwrite(&ctemp,1,1,file);
	}

	fclose(file);
}
