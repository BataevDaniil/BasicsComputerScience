
#include <iostream>
#include <fstream>

#define show(x) std::cout << #x << " = " << x << std::endl;

template <typename T>
T minArr(T *arr, int len)
{
	T min = arr[0];
	for (int i = 0; i < len; i++)
		if (arr[i] < min)
			min = arr[i];
	return min;
};

int main()
{
	std::ifstream read("input.txt", std::ofstream::in);
	if (!read.is_open())
		std::cout << "not can read file" << std::endl;
	char buf[100];
	read >> buf; read >> buf;
	read >> buf; int len; read >> len;
	double arr[len];
	for (int i = 0; i < len; i++)
		read >> arr[i];
	read >> buf; read >> buf; read >> buf; read >> buf;

	std::ofstream output("out.txt");
	output << "array" << std::endl;
	for (int i = 0; i < len; i++)
		output << arr[i] << ' ';
	output << std::endl;
	output << "minArr= " << minArr<int>(arr, len);
};